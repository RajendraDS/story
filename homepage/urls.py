from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('profile/', views.profile, name='profile'),
    path('portofolio/',views.portofolio, name='portofolio'),
    path('gallery/',views.gallery, name='gallery'),
    path('project/',views.project, name='project'),
    path('project/story1',views.story1, name='story1'),
    path('project/comingsoon',views.comingsoon, name='comingsoon'),
    # dilanjutkan ...
]