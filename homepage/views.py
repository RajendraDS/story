from django.shortcuts import render
def index(request):
    return render(request, 'index.html')

def profile(request):
    return render(request, 'profile.html')

def portofolio(request):
    return render(request, 'portofolio.html')

def gallery(request):
    return render(request, 'gallery.html')

def project(request):
    return render(request, 'project.html')

def story1(request):
    return render(request, 'story1.html')

def comingsoon(request):
    return render(request, 'comingsoon.html')
# Create your views here.
