$( document ).ready(function() {
    getBooks()
});

$('#queryBook').keyup(() => {
    getBooks()
})

const getBooks = () => {
    const keyword = $('#queryBook').val();

    fetch(`/data/?` + new URLSearchParams({querryBook: keyword}))
        .then(response => response.json())
        .then(responseJson => {
            if (responseJson[0].error) showResponseMessage(responseJson[0].error)
            else renderAllBooks(responseJson);
        })
        .catch(error => {
            showResponseMessage(error);
        })
}

const renderAllBooks = (books) => {
    $('tbody').empty();
    let data = "";

    books.forEach(book => {
        data += `
            <tr>
                <td><img class="book-cover" src="${book.imageLinks}"></td>
                <td>${book.title}</td>
                <td>${book.authors}</td>
                <td>${book.publisher}</td>
                <td>${book.categories}</td>
            </tr>
        `
    });
    $('tbody').append(data);
};

const showResponseMessage = (message) => {
    $('tbody').empty();
    const data = `
        <tr>
            <td colspan="5" style="text-align: center;">${message}</td>
        </tr>
    `
    $('tbody').append(data);
}
