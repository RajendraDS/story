from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def showBook(request):
    html = 'story8.html'
    return render(request, html)

def showBook(request):
    html = 'story8.html'
    return render(request, html)
    
def process(data):
    books = data["items"] if "items" in data else ''
    booksJson = []
    if books != '':
        for book in books:
            content = book["volumeInfo"]

            imageLinks = content["imageLinks"] if "imageLinks" in content else ''
            imageUrl = imageLinks["smallThumbnail"] if imageLinks != '' else ''

            title = content["title"] if "title" in content else ''
            
            authors = content["authors"] if "authors" in content else ''
            authorData = ''
            if (authors != '') :
                for author in authors[:-1]:
                    authorData += author + ', '
                authorData += authors[-1]
            else:
                authorData = '-'

            publisher = content["publisher"] if "publisher" in content else ''
            publisher = publisher if publisher != '' else '-'

            categories = content["categories"] if "categories" in content else '-'

            bookDict = {
                "imageLinks": imageUrl,
                "title": title,
                "authors": authorData,
                "publisher": publisher,
                "categories": categories
            }

            booksJson.append(bookDict)

    else:
        booksJson.append({"error": "Mohon maaf buku tidak dapat ditemukan"})
        
    return booksJson

def dataBook(request):
    querry = request.GET.get('querryBook')
    querry = querry if querry != '' else 'Adbis'
    request_url = 'https://www.googleapis.com/books/v1/volumes?q=' + querry
    book_request = requests.get(request_url)
    data = json.loads(book_request.content)
    bookJson = process(data)
    return JsonResponse(bookJson, safe=False)