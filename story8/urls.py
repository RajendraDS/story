from django.urls import path
from . import views

app_name = 'story8'
urlpatterns = [
    path('story8/', views.showBook, name="story8"),
    path('data/', views.dataBook, name="data"),
]
