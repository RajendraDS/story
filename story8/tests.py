from django.test import TestCase, Client
from http import HTTPStatus
from django.urls import resolve
from .views import showBook, dataBook, process

# Create your tests here.
class Story8Test(TestCase):

    # Test URL
    def test_story8_url(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)
    def test_story8_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, showBook)

    def test_get_data_no_querry(self):
        response = Client().get('/data/', {'querryBook': ''})
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_get_data(self):
        response = Client().get('/data/', {'querryBook': 'test'})
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_API_return_no_data(self):
        response = Client().get('/data/', {'querryBook': 'fadskjfjsadffs'})
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            [{"error": "Mohon maaf buku tidak dapat ditemukan"}])