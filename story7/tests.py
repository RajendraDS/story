from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .views import story7

# Create your tests here.
class Story7Test(TestCase):
    # Test URL
    def test_url_aktivitas (self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_activity_using_activity_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7)

    def test_template_tambah (self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response,'story7.html')