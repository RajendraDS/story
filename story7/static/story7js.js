$( document ).ready(function() {
    $( "#accordion" ).accordion({
        collapsible: true,
        heightStyle: "content"
    });

    $("i.down").click(function(event) {
        event.stopPropagation();
        const theParent = $(this).parent().get(0);
        const thisHeader = $(theParent);
        const thisContent = $(theParent).next();
        const nextContent = $(theParent).next().next().next();
        if(nextContent.html() === undefined) {
            const firstHeader = $(theParent).parent().children().first();
            thisHeader.insertBefore(firstHeader);
        } else {
            thisHeader.insertAfter(nextContent);
        }
        thisContent.insertAfter(thisHeader);
    });

    $("i.up").click(function(event) {
        event.stopPropagation();
        const theParent = $(this).parent().get(0);
        const thisHeader = $(theParent);
        const thisContent = $(theParent).next();
        const prevHeader = $(theParent).prev().prev();
        if(prevHeader.html() === undefined) {
            const lastContent = $(theParent).parent().children().last();
            thisHeader.insertAfter(lastContent);
        } else {
            thisHeader.insertBefore(prevHeader);
        }
        thisContent.insertAfter(thisHeader);
    });
});
