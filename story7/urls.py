from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('', views.story7, name='story7'),
    # path('profile/', views.profile, name='profile'),
    # dilanjutkan ...
]