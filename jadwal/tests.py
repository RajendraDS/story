from django.test import Client, TestCase
# Mengimport models mata kuliah
from .models import Jadwal
class Story5UnitTest(TestCase):
    # def test_setUpClass(self):
    #     mata_kuliah = Jadwal.objects.create(matkul='nama_mata_kuliah')
        

    def test_setUpClass(self):
        mata_kuliah = Jadwal.objects.create (
            matkul = 'nama_mata_kuliah',
            dosen = 'nama_dosen',
            sks = 3,
            semester = 'Gasal 2020/2021',
            deskripsi = 'isian_deskripsi',
            kelas = 'nomor_kelas'
        )
        self.assertTrue(isinstance(mata_kuliah, Jadwal))
        self.assertEqual('nama_mata_kuliah', str(mata_kuliah))
    

    def test_url_home_ada (self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        

    def test_url_tambah_matkul_ada (self):
        response = Client().get('/jadwal/tambah/?submit=True')
        self.assertEqual(200,response.status_code)
    
    # Template test (URL)
    def test_template_tambah (self):
        response = Client().get('/jadwal/')
        self.assertTemplateUsed(response,'list_jadwal.html')

    # cek view pada tambah matkul (view)
    def test_views_tambah (self):
        response = self.client.post('/jadwal/tambah/?submit=True', data= {
            'matkul' : 'unit test',
            'dosen' : 'unit test',
            'sks' : 3,
            'semester' : 'Gasal 2020/2021',
            'deskripsi' : 'unit test',
            'kelas' : 'unit test',
        })
        self.assertEquals(Jadwal.objects.all().count(),1)
        primary_key = Jadwal.objects.first()

    # def test_model_tambah (self):
    #     mata_kuliah = Jadwal.objects.get(matkul='nama_jadwal_baru')
    #     self.assertEqual(mata_kuliah.matkul, 'nama_matkul_baru')
    #     self.assertEqual(mata_kuliah.dosen, 'nama_dosen')
    #     self.assertEqual(mata_kuliah.sks, 3)
    #     self.assertEqual(mata_kuliah.semester,'Gasal 2020/2021')
    #     self.assertEqual(mata_kuliah.deskripsi,'isian_deskripsi')
    #     self.assertEqual(mata_kuliah.kelas,'nomor_kelas')