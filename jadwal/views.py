from django.shortcuts import redirect, render
from django.http import HttpResponseRedirect
from .models import Jadwal
from .forms import SubjectForm
import datetime


def jadwal(request):
    if request.method == "POST":
        Jadwal.objects.get(id=request.POST['id']).delete()
        return redirect('/jadwal/')
    list_jadwal = Jadwal.objects.all()
    return render(request, 'list_jadwal.html', {'list_jadwal': list_jadwal})

def tambah_jadwal(request):
    submit = False
    if request.method == 'POST':
        form = SubjectForm(request.POST)
        if form.is_valid():
            form.save()
            # return render(request, 'subject_add.html')
            return HttpResponseRedirect('/jadwal/tambah/?submit=True')
    else:
        form = SubjectForm()
        if 'submit' in request.GET:
            submit = True
    return render(request, 'tambah_jadwal.html', {'form': form, 'submit': submit})


def detail_jadwal(request, index):
    jadwal = Jadwal.objects.get(pk=index)
    return render(request, 'detail_jadwal.html', {'jadwal': jadwal})


