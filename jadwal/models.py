from django.db import models
# Create your models here.
SEMESTER_CHOICES = ( 
    ("Gasal 2020/2021", "Gasal 2020/2021"),
    ("Genap 2019/2020", "Genap 2019/2020"), 
    ("Gasal 2019/2020", "Gasal 2019/2020"),
    ("dll", "dll"),
) 

# ini buat detail jadwal yang ada
class Jadwal(models.Model):
    matkul = models.CharField('Matakuliah', max_length=120, null=True)
    dosen = models.CharField('Dosen', max_length=120, null=True)
    sks = models.IntegerField('SKS', null=True)
    
    semester = models.CharField('Semester', 
    max_length=120, null=True,
    choices = SEMESTER_CHOICES, 
    default = 'Gasal 2020/2021'
    )  

    deskripsi = models.TextField('Deskripsi', max_length=100, null=True)
    kelas = models.CharField('Kelas', max_length=120, null=True, default='Kamar Pribadi')

    def __str__(self):
        return self.matkul
