from django.urls import path
from . import views

app_name = 'jadwal'

urlpatterns = [
    path('', views.jadwal, name='jadwal'),
    path('tambah/', views.tambah_jadwal, name='tambah_jadwal'),
    path('detail/<int:index>/', views.detail_jadwal, name='detail_jadwal'),

    # dilanjutkan ...
]