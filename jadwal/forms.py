from django.forms import ModelForm
from .models import Jadwal


class SubjectForm(ModelForm):
    required_css_class = 'required'
# fields that are automatically generated depend on the content of the 
# Meta class and on which fields have already been defined declaratively
    class Meta:
        model = Jadwal
        fields = '__all__'

